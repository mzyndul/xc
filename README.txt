INSTALL:
========

create virtualenv
cd to the project
pip install -r requirements/dev.txt
export DJANGO_SETTINGS_MODULE='xc.settings.dev'
cd project
python manage.py migrate
python manage.py runserver