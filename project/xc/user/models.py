from django.contrib.auth.models import AbstractUser
from django.db.models import Sum


class User(AbstractUser):
    def get_spent_since(self, from_date):
        from xc.bet.models import Bet
        return Bet.objects.filter(wallet__user=self, timestamp__gte=from_date).aggregate(spent=Sum('amount'))['spent']
