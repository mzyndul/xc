from django.test import TestCase
from django.urls import reverse

from xc.bet.models import Bet
from xc.bonus.tests import create_user
from xc.wallet.models import Wallet


class TestHomeView(TestCase):

    def setUp(self):
        self.user = create_user()

    def test_invalid_deposit(self):
        self.client.login(username='a', password='a')
        response = self.client.post(reverse('home'), {
            'amount': 'abc'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context.get('messages')), 1)
        self.assertEqual(Wallet.objects.all().count(), 0)

    def test_valid_deposit(self):
        self.client.login(username='a', password='a')
        response = self.client.post(reverse('home'), {
            'amount': 10
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context.get('messages')), 1)
        self.assertEqual(Wallet.objects.all().count(), 1)

    def test_spin_no_funds(self):
        self.client.login(username='a', password='a')
        response = self.client.post(reverse('home'), {})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Bet.objects.all().count(), 0)

    def test_spin(self):
        self.client.login(username='a', password='a')
        wallet = Wallet.objects.get_real_wallet(self.user)
        wallet.amount = 100
        wallet.save()
        response = self.client.post(reverse('home'), {})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Bet.objects.all().count(), 1)
