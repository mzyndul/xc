from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _

from xc.user.models import User


class SignupForm(forms.ModelForm):
    password = forms.CharField(max_length=16, widget=forms.PasswordInput())
    password_confirm = forms.CharField(max_length=16, widget=forms.PasswordInput())

    def clean(self):
        cd = self.cleaned_data
        if 'password' in cd and 'password_confirm' in cd:
            if cd['password'] != cd['password_confirm']:
                raise ValidationError(_('Passwords are not equal'))
            return cd
        raise ValidationError(_('Password is required'))

    def save(self, commit=True):
        obj = super().save(commit=True)
        obj.set_password(self.cleaned_data['password'])
        obj.save()
        return obj

    class Meta:
        model = User
        fields = ['username', 'password', 'password_confirm']
