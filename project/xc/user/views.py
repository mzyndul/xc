from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.urls.base import reverse_lazy
from django.views.generic import CreateView, TemplateView
from django.utils.translation import ugettext as _

from xc.bet.models import Bet
from xc.bonus.models import Bonus
from xc.deposit.forms import DepositForm
from xc.user.forms import SignupForm
from xc.user.models import User
from xc.wallet.models import Wallet


class SignupView(CreateView):
    model = User
    form_class = SignupForm
    template_name = 'registration/signup.html'
    success_url = reverse_lazy('login')


class XCLoginView(LoginView):
    def form_valid(self, form):
        user = form.get_user()
        if not user.last_login:
            Bonus.objects.process(Bonus.EVENT_FIRST_LOGIN, user)
        return super().form_valid(form)


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        u = super().get_context_data()
        u.update({
            'form_deposit': DepositForm(),
            'amount_eu': '',
            'amount_bns': ''
        })
        return u

    def process_deposit(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = DepositForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.wallet = Wallet.objects.get_real_wallet(user=self.request.user)
            obj.wallet.update_wallet(obj.amount)
            obj.save()
            Bonus.objects.process(Bonus.EVENT_DEPOSIT, self.request.user, deposit=obj)
            messages.success(self.request, _('Deposit done.'))
        else:
            context['form_deposit'] = form
            messages.error(self.request, _('Deposit done.'))
        return self.render_to_response(context)

    def process_spin(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        wallet = Wallet.objects.get_wallet_for_bet(user=request.user, amount=settings.SPIN_AMOUNT)
        if not wallet:
            messages.error(self.request, _('Not enough funds'))
        else:
            b = Bet.objects.spin(wallet=wallet)
            result = '' if b.win else '-'
            messages.info(self.request, _("Spin result: {}{}").format(result, b.amount))
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        if 'amount' in request.POST:
            return self.process_deposit(request, *args, **kwargs)
        else:
            return self.process_spin(request, *args, **kwargs)
