from unittest.mock import patch

from django.conf import settings
from django.test import TestCase

from xc.bet.models import Bet
from xc.bonus.tests import create_user
from xc.wallet.models import Wallet


class TestSpin(TestCase):

    @patch('random.randint')
    def test_spin(self, ri):
        ri.return_value = 1
        wallet = Wallet.objects.get_real_wallet(create_user())
        Bet.objects.spin(wallet)
        self.assertEqual(Bet.objects.all().count(), 1)
        b = Bet.objects.get()
        self.assertEqual(b.amount, settings.SPIN_AMOUNT)
        wallet.refresh_from_db()
        self.assertEqual(wallet.amount, 2)

