import random

from django.conf import settings
from django.db import models
from django.db.transaction import atomic

from xc.bonus.models import WalletBonus
from xc.wallet.models import Wallet


class SpinManager(models.Manager):

    @atomic
    def spin(self, wallet):
        result = bool(random.randint(0, 1))
        obj = Bet.objects.create(
            wallet=wallet,
            amount=settings.SPIN_AMOUNT,
            win=result
        )
        amount = settings.SPIN_AMOUNT if result else -settings.SPIN_AMOUNT
        wallet.update_wallet(amount)
        WalletBonus.objects.process_bonuses(wallet.user)
        return obj


class Bet(models.Model):
    wallet = models.ForeignKey(Wallet)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    win = models.BooleanField()
    timestamp = models.DateTimeField(auto_now_add=True)

    objects = SpinManager()
