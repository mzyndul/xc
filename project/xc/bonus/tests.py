from decimal import Decimal
from datetime import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from xc.bet.models import Bet
from xc.bonus.exceptions import BonusException
from xc.bonus.models import Bonus, WalletBonus
from xc.user.models import User
from xc.wallet.models import Wallet


def create_user():
    u = User(username='a')
    u.set_password('a')
    u.is_active = True
    u.save()
    return u


def create_bonus(wt=Wallet.TYPE_EURO):
    b = Bonus()
    b.event = Bonus.EVENT_DEPOSIT
    b.wagering_requirement = 10
    b.bonus_amount = 100
    b.wallet_type = wt
    b.save()
    return b


class BaseBonusTest(TestCase):
    def test_process_not_existing_bonus(self):
        user = create_user()
        b = Bonus()
        b.event = 5
        self.assertRaises(BonusException, b.process,user)


class BonusTestFirstLogin(TestCase):

    def setUp(self):
        self.user = create_user()

    def test_first_login(self):
        response = self.client.post(reverse('login'), {
            'username': 'a',
            'password': 'a'
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.user.wallet_set.all().count(), 1)
        wallet = self.user.wallet_set.get()
        self.assertEqual(wallet.wallet_type, Wallet.TYPE_EURO)
        self.assertEqual(wallet.amount, Decimal(100))

    def test_second_login(self):
        self.user.last_login = timezone.now()
        self.user.save()
        response = self.client.post(reverse('login'), {
            'username': 'a',
            'password': 'a'
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.user.wallet_set.all().count(), 0)


class BonusTestDeposit(TestCase):

    def setUp(self):
        self.user = create_user()
        self.client.login(username='a', password='a')

    def test_add_deposit_below_requirement(self):
        response = self.client.post(reverse('home'), {
            'amount': 10
        })
        self.assertEqual(response.status_code, 200)
        wallet = self.user.wallet_set.get()
        self.assertEqual(wallet.amount, 10)
        self.assertEqual(wallet.wallet_type, wallet.TYPE_EURO)
        self.assertEqual(WalletBonus.objects.filter(user=self.user).count(), 0)

    def test_add_deposit_above_requirement(self):
        response = self.client.post(reverse('home'), {
            'amount': 200
        })
        self.assertEqual(response.status_code, 200)
        # we expect two wallets, real and bonus
        self.assertEqual(self.user.wallet_set.all().count(), 2)
        real_wallet = Wallet.objects.get_real_wallet(self.user)
        bonus_wallet = Wallet.objects.get_bonus_wallet(self.user)
        self.assertEqual(real_wallet.amount, 200)
        self.assertEqual(bonus_wallet.amount, 20)
        self.assertEqual(WalletBonus.objects.filter(user=self.user).count(), 1)


class Test(TestCase):
    def setUp(self):
        self.user = create_user()
        self.wallet = Wallet.objects.get_bonus_wallet(self.user)
        self.bonus = create_bonus(wt=Wallet.TYPE_BONUS)
        self.wallet_bonus = self.bonus._create_bonus_wallet(self.user)
        self.b = Bet(
            wallet=self.wallet,
            amount=10,
            win=True
        )
        self.b.save()

    def test_low_wagering(self):
        WalletBonus.objects.process_bonuses(self.user)
        self.assertEqual(Wallet.objects.all().count(), 1)

    def test_wagering(self):
        self.b.amount = 1500
        self.b.save()
        self.assertEqual(self.wallet.amount, 0)
        WalletBonus.objects.process_bonuses(self.user)
        self.assertEqual(Wallet.objects.all().count(), 2)
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 0)
        rw = Wallet.objects.get_real_wallet(self.user)
        self.assertEqual(rw.amount, 100)

    def test_wagering_the_same_bonus(self):
        self.b.amount = 1500
        self.b.save()
        self.wallet_bonus.processed = timezone.now()
        self.wallet_bonus.save()
        self.assertEqual(self.wallet.amount, 0)
        WalletBonus.objects.process_bonuses(self.user)
        self.assertEqual(Wallet.objects.all().count(), 1)
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.amount, 0)

    def test_wagering_real_bonus(self):
        self.b.amount = 1500
        self.b.save()
        self.wallet_bonus.wallet_type = Wallet.TYPE_EURO
        self.wallet_bonus.save()
        self.assertEqual(self.wallet.amount, 0)
        WalletBonus.objects.process_bonuses(self.user)
        self.assertEqual(Wallet.objects.all().count(), 1)
