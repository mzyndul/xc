import json
from decimal import Decimal
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.transaction import atomic
from django.utils import timezone

from xc.bonus.exceptions import BonusException
from xc.user.models import User
from xc.wallet.models import Wallet


class BonusManager(models.Manager):
    def process(self, event, user, **params):
        for b in self.get_queryset().filter(event=event):
            b.process(user, **params)


class Bonus(models.Model):

    EVENT_FIRST_LOGIN = 1
    EVENT_DEPOSIT = 2
    EVENTS = (
        (EVENT_FIRST_LOGIN, 'First login'),
        (EVENT_DEPOSIT, 'Deposit'),
    )

    wagering_requirement = models.PositiveIntegerField(validators=[
        MinValueValidator(1), MaxValueValidator(100)
    ])
    bonus_amount = models.DecimalField(max_digits=10, decimal_places=2)
    event = models.SmallIntegerField(choices=EVENTS)
    params = models.TextField(null=True, blank=True)
    wallet_type = models.SmallIntegerField(Wallet.WALLET_TYPE)

    objects = BonusManager()

    @atomic
    def process(self, user, **params):
        # we create copy of bonus for specific user,
        # we duplicate some of the variables, so that it is posible to change bonus for all new plaers, but keep
        # old values for old user.

        try:
            f = getattr(self, "process_{}".format(self.event))
        except AttributeError:
            raise BonusException()
        f(user, **params)

    def process_1(self, user, **params):
        self._create_bonus_wallet(user)
        wallet = Wallet.objects.get_wallet(user, self.wallet_type)
        wallet.update_wallet(self.bonus_amount)

    def process_2(self, user, **params):
        deposit = params['deposit']
        custom_params = json.loads(self.params)
        if deposit.amount >= Decimal(custom_params['deposit']):
            self._create_bonus_wallet(user)
            wallet = Wallet.objects.get_wallet(deposit.wallet.user, self.wallet_type)
            wallet.update_wallet(self.bonus_amount)

    def _create_bonus_wallet(self, user):
        wb = WalletBonus()
        wb.user = user
        wb.bonus = self
        wb.amount = self.bonus_amount
        wb.wallet_type = self.wallet_type
        wb.wagering_requirement = self.wagering_requirement
        wb.save()
        return wb


class WalletBonusManager(models.Manager):
    def process_bonuses(self, user):
        # I was not sure how to process replacement BNS to EU, so below I'm processing only one bonus after each spin
        wb = self.get_queryset().filter(user=user, processed__isnull=True, wallet_type=Wallet.TYPE_BONUS).first()
        if not wb:
            return
        spent = user.get_spent_since(wb.timestamp)
        if spent >= wb.get_cash_in_requirement():
            wb.deposit_bonus()


class WalletBonus(models.Model):
    user = models.ForeignKey(User)
    bonus = models.ForeignKey(Bonus)
    timestamp = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    processed = models.DateTimeField(null=True, blank=True)
    wallet_type = models.SmallIntegerField(choices=Wallet.WALLET_TYPE)
    wagering_requirement = models.PositiveIntegerField(validators=[
        MinValueValidator(1), MaxValueValidator(100)
    ])

    objects = WalletBonusManager()

    def get_cash_in_requirement(self):
        return self.wagering_requirement * self.amount

    @atomic
    def deposit_bonus(self):
        if self.wallet_type == Wallet.TYPE_BONUS:
            wallet = Wallet.objects.get_real_wallet(self.user)
            wallet.update_wallet(self.amount)
            self.processed = timezone.now()
            self.save()
