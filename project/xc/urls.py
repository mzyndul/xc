from django.conf import settings
from django.conf.urls import url
from django.contrib.auth.views import LogoutView

from xc.user.views import SignupView, HomeView, XCLoginView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^login/$', XCLoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^signup$', SignupView.as_view(), name='signup'),
]

if settings.DEBUG_URLS:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
