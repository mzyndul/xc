"""Development settings and globals."""
from .base import *

DEBUG = True
DEBUG_URLS = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': normpath(join(DJANGO_ROOT, 'default.db')),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}


INTERNAL_IPS = ('127.0.0.1', '0.0.0.0', '*')
