from django import forms

from xc.deposit.models import Deposit


class DepositForm(forms.ModelForm):

    def save(self, commit=True, wallet=None):
        self.instance.wallet = wallet
        return super().save(commit)

    class Meta:
        model = Deposit
        fields = ['amount']
