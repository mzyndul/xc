from django.db import models

from xc.wallet.models import Wallet


class Deposit(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField('Deposit amount', max_digits=10, decimal_places=2)
    wallet = models.ForeignKey(Wallet)
