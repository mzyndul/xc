from django.db import models
from django.utils.translation import ugettext as _

from xc.user.models import User
from xc.wallet.exceptions import WalletException


class WalletManager(models.Manager):

    def get_wallet_for_bet(self, user, amount):
        """
        Return first wallet from which we can make a bet, order is as follow, first real money, then bonus money,
        if each wallet has less than required bet then we return False, even if sum of all wallets if >= of bet
        :param user: user instance
        :param amount: bet amount
        :return: wallet instance
        """
        return self.get_queryset().filter(
            user=user, amount__gte=amount, is_depleted=False
        ).order_by('wallet_type').first()

    def get_wallet(self, user, wallet_type):
        assert wallet_type in [Wallet.TYPE_BONUS, Wallet.TYPE_EURO]
        if wallet_type == Wallet.TYPE_BONUS:
            wallet = self.get_queryset().filter(
                user=user, wallet_type=wallet_type, amount__gt=0, is_depleted=False).first()
        else:
            wallet = self.get_queryset().filter(user=user, wallet_type=wallet_type).first()
        if not wallet:
            wallet = Wallet(user=user, wallet_type=wallet_type, amount=0)
            wallet.save()
        return wallet

    def get_real_wallet(self, user):
        return self.get_wallet(user=user, wallet_type=Wallet.TYPE_EURO)

    def get_bonus_wallet(self, user):
        return self.get_wallet(user=user, wallet_type=Wallet.TYPE_BONUS)


class Wallet(models.Model):

    TYPE_EURO = 1
    TYPE_BONUS = 2

    WALLET_TYPE = (
        (TYPE_BONUS, _('Bonus wallet')),
        (TYPE_EURO, _('Euro wallet')),
    )

    user = models.ForeignKey(User)
    wallet_type = models.SmallIntegerField(choices=WALLET_TYPE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    is_depleted = models.BooleanField(default=False)

    objects = WalletManager()

    def update_wallet(self, amount):
        if amount > 0:
            # cannot change depleated wallet
            if self.is_depleted:
                raise WalletException()
        self.amount = self.amount + amount
        if self.amount == 0 and self.wallet_type == Wallet.TYPE_BONUS:
            self.is_depleted = True
        self.save()
