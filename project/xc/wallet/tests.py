from django.test import TestCase

from xc.bonus.tests import create_user
from xc.wallet.exceptions import WalletException
from xc.wallet.models import Wallet


class TestGetWalletForBet(TestCase):

    def setUp(self):
        self.user = create_user()
        self.rw = Wallet.objects.get_real_wallet(self.user)
        self.bw = Wallet.objects.get_bonus_wallet(self.user)
        self.rw.amount = 100
        self.rw.save()
        self.bw.amount = 100
        self.bw.save()

    def test_get_real_wallet_for_bet(self):
        w = Wallet.objects.get_wallet_for_bet(self.user, 5)
        self.assertEqual(w.wallet_type, Wallet.TYPE_EURO)

    def test_get_bonus_wallet_for_bet(self):
        self.rw.amount = 2
        self.rw.save()
        w = Wallet.objects.get_wallet_for_bet(self.user, 5)
        self.assertEqual(w.wallet_type, Wallet.TYPE_BONUS)

    def test_get_bonus_wallet_for_bet_if_it_is_marked_as_depleted(self):
        self.rw.amount = 0
        self.rw.save()
        self.bw.amount = 0
        self.bw.is_depleted = True
        self.bw.save()
        w = Wallet.objects.get_wallet_for_bet(self.user, 5)
        self.assertFalse(w)

    def test_get_bonus_when_no_cash(self):
        self.rw.amount = 0
        self.rw.save()
        self.bw.amount = 0
        self.bw.save()
        w = Wallet.objects.get_wallet_for_bet(self.user, 5)
        self.assertFalse(w)

    def test_update_depleted_wallet(self):
        self.bw.amount = 0
        self.bw.is_depleted = True
        self.bw.save()
        self.assertRaises(WalletException, self.bw.update_wallet, 10)

    def test_if_wallet_will_be_marked_as_depleted(self):
        self.bw.amount = 5
        self.bw.is_depleted = True
        self.bw.save()
        self.bw.update_wallet(-5)
        self.bw.refresh_from_db()
        self.assertTrue(self.bw.is_depleted)